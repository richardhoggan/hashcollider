# README #

###Read Me Introduction
This read me discusses a brief overview of the HashCollider application, it's available features, development attributes, and how to work with the application.

###Project Description
HashCollider is a tool designed to break hashed passwords.  In most instances, passwords are still hashed in MD5 and SHA1, both of which are considered insecure hashing algorithms.  To this end, HashCollider is designed to break both salted and unsalted versions of both types of hashes.  Additionally, support is being provided for SHA-256 hashing.  

It's also important to note that in most cases the quality of the password dictates how easy it will be to break.  For example, cats and popcorn are simple one word passwords which ultimately are still discovered in the wild.  As such, the master passwords file will include the most commonly used passwords but is further destined for additional growth.  

Finally, there are many tools available, and from this perspective, HashCollider is designed with the explicit purpose of being an available tool with plans for future development.  

###HashCollider Features
HashCollider provides the following features:  

* Analysis - Using the analysis module, a potential calculation can run which indicates how many potential attempts the application will use to break a given hash value.
* Salted/Un-salted Hash Cracking - HashCollider currently has features for cracking MD5, SHA-1, and SHA-256 hashed values.  

###Development Attributes
HashCollider was developed using Python 2.7, and was designed to be a cross platform application able to run on the "big three" operating systems.  Additional support is also provided for the following mobile operating systems:

* IOS - Using the Pythonista.
* Android - Using PyDroid - IDE for Python 2.

** Please note that mobile versions of the application code are being tested and will be added as soon as testing completes.

###Running HashCollider
Finally, for desktop versions of the application, simply run the application at the command line using the HashCollider.py file within the associated application directory.