#Import directives
import FileIO
import HashingHandler

#Function Name: md5CollisionExecutionHandler()
#Function Description: Iterates through all passwords in the master password list and
#attempts to hash them as MD5 hash digests then conducts a comparison.
def collisionExecutionHandler(hashValue, hashingDigest):
    #Variable declarations
    masterPasswordsList = FileIO.readFile("MasterPasswordsList.txt")
    masterPasswordsListLength = 0
    currentPassword = ""
    currentPasswordHash = ""
    currentPasswordIndex = 0
    collisionState = []

    #Get the length of the passwords file and verify there is content
    masterPasswordListLength = len(masterPasswordsList)
    if masterPasswordListLength > 0:
        #Iterate through the master passwords list and attempt to break
        while currentPasswordIndex < masterPasswordListLength:
            #Get the current password and encrypt
            currentPassword = masterPasswordsList[currentPasswordIndex]
            currentPassword = currentPassword.strip()

            #Determine which hashing digest was chosen and pass to the appropriate handler
            if hashingDigest == "MD5":
                currentPasswordHash = HashingHandler.hashAsMD5(currentPassword)
            elif hashingDigest == "SHA1":
                currentPasswordHash = HashingHandler.hashAsSHA1(currentPassword)
            elif hashingDigest == "SHA256":
                currentPasswordHash = HashingHandler.hashAsSHA256(currentPassword)

            #Determine if the currentPasswordHash matches the hashValue
            if hashValue == currentPasswordHash:
                #If the hash value and current password hash value match then generate the
                #collision state information and prepare to return
                collisionOccurredFlag = True
                collisionState.append(collisionOccurredFlag)
                collisionState.append(currentPassword)
                return collisionState

            #Increment the current password index
            currentPasswordIndex = currentPasswordIndex + 1

    #If execution reaches here then return false collision state information
    collisionState.append(False)
    return collisionState