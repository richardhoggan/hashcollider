class CLInputOutputAttributes:
    #Class constructors
    def __init__(self):
        self.totalCommandLineArguments = 0
        self.operationMode = ""
        self.fileMode = ""
        self.username = ""
        self.passwordHash = ""
        self.brokenPasswordHash = ""
        self.brokenPasswordHashesList = []
        self.commandLineArgumentsValidated = False

    #Setters and getters
    def setTotalCommandLineArguments(self, totalCommandLineArguments):
        self.totalCommandLineArguments = totalCommandLineArguments

    def setOperationMode(self, operationMode):
        self.operationMode = operationMode

    def setFileMode(self, fileMode):
        self.fileMode = fileMode

    def setUsername(self, username):
        self.username = username

    def setPasswordHash(self, passwordHash):
        self.passwordHash = passwordHash

    def setBrokenPasswordHash(self, brokenPasswordHash):
        self.brokenPasswordHash = brokenPasswordHash

    def setBrokenPasswordHashesList(self, brokenPasswordHashesList):
        self.brokenPasswordHashesList = brokenPasswordHashesList

    def setCommandLineArgumentsValidated(self, commandLineArgumentsValidated):
        self.commandLineArgumentsValidated = commandLineArgumentsValidated

    def getTotalCommandLineArguments(self):
        return self.totalCommandLineArguments

    def getOperationMode(self):
        return self.operationMode

    def getFileMode(self):
        return self.fileMode

    def getUsername(self):
        return self.username

    def getPasswordHash(self):
        return self.passwordHash

    def getBrokenPasswordHash(self):
        return self.brokenPasswordHash

    def getBrokenPasswordHashesList(self):
        return self.brokenPasswordHashesList

    def getCommandLineArgumentsValidated(self):
        return self.commandLineArgumentsValidated