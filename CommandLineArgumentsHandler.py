from CLInputOutputAttributes import CLInputOutputAttributes

def commandLineArgumentsHandler(commandLineArgumentsList):
    #Variable declarations
    commandLineArgumentsListLength = 0
    attributesObject = CLInputOutputAttributes()

    #Parse through the command line arguments and validate for correctness
    #Validate command line arguments based on the type of operation being performed
    #All non-salted operations are length 3
    commandLineArgumentsListLength = len(commandLineArgumentsList)
    if commandLineArgumentsListLength == 3:
        #Set the total number of command line arguments
        attributesObject.setTotalCommandLineArguments(commandLineArgumentsListLength)

        #Determine which option mode is being requested
        if commandLineArgumentsList[1] == "-md5":
            attributesObject.setOperationMode("-md5")
            attributesObject.setCommandLineArgumentsValidated(True)
        elif commandLineArgumentsList[1] == "-sha1":
            attributesObject.setOperationMode("-sha1")
            attributesObject.setCommandLineArgumentsValidated(True)
        elif commandLineArgumentsList[1] == "-sha256":
            attributesObject.setOperationMode("-sha256")
            attributesObject.setCommandLineArgumentsValidated(True)
        else:
            #Immediately set the validation flag to false if the operation mode
            #was not not one expected
            attributesObject.setCommandLineArgumentsValidated(False)

        #Determine if the user provided a username/password hash combination or just the password hash
        usernamePasswordHash = commandLineArgumentsList[2]
        if "," in usernamePasswordHash:
            usernamePasswordHashSplit = usernamePasswordHash.split(",")
            attributesObject.setUsername(usernamePasswordHashSplit[0])
            attributesObject.setPasswordHash(usernamePasswordHashSplit[1])
            attributesObject.setCommandLineArgumentsValidated(True)
        elif len(usernamePasswordHash) == 32 or len(usernamePasswordHash) == 40 \
            or len(usernamePasswordHash) == 64:
            attributesObject.setPasswordHash(usernamePasswordHash)
            attributesObject.setCommandLineArgumentsValidated(True)
        else:
            attributesObject.setCommandLineArgumentsValidated(False)

        #Determine if commandLineArgumentsList[2] contains the fileMode flag or a username/password combination
        commandSwitch = commandLineArgumentsList[2]
        if commandSwitch == "-file":
            attributesObject.setFileMode("-file")
            attributesObject.setCommandLineArgumentsValidated(True)
    else:
        #Immediately set the validation flag to false if the wrong number of command
        #line arguments was found
        attributesObject.setCommandLineArgumentsValidated(False)

    #Return attributeObject on parse completion
    return attributesObject