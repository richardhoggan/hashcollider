import time

#Method Name: getCPUClockTime()
#Method Description: Gets the current CPU clock time and returns its value.
def getCPUClockTime():
    return time.clock()

#Method Name: calculateDifference()
#Method Description: Takes the starting and ending CPU clock times as input and returns their difference,
def calculateDifference(startingTime, endingTime):
    return (endingTime - startingTime)