#Program: HashCollider.py
#Developer: Rich Hoggan
#Creation Date: 04/05/2016
#Description: Attempts to break passwords which are hashed using MD5, SHA1, SHA256 hashing algorithms.  Also
#attempts to break passwords which are hashed using salts and multiple rounds.

#jbridger,d334cb97f2f6f07cabbfad0b2d87d6195110f1cd811cc1d618167c7b0fd87594

#System Import directives
import sys
from CLInputOutputAttributes import CLInputOutputAttributes
import CommandLineArgumentsHandler

#Application Import Directives
import Analysis
import FileIO
import TimingUtilities
import CollisionExecutionHandler

#Method Code:
#Method Name: printProgramHeader()
#Method Description: Prints the program's information header during normal
#exeuction.
def printProgramHeader():
    print "HashCollider.py"
    print "Developed By: Rich Hoggan"
    print ""

#Method Name: printUsage()
#Method Description: Prints a usage statement.
def printUsage():
    print "HashCollider.py"
    print "Developed By: Rich Hoggan"
    print ""
    print "Command Line Usage:"
    print "Non-Salted Hashing:"
    print "HashCollider.py -md5 <[username,]hash>"
    print "\tThe value of <[username,]hash> will be passed to the non-salted MD5 handler."
    print "HashCollider.py -sha1 <[username,]hash>"
    print "\tThe value of <[username,]hash> will be passed to the non-salted SHA-1 handler."
    print "HashCollider.py -sha256 <[username,]hash>"
    print "\tThe value of <[username,]hash> will be passed to the non-salted SHA-256 handler."
    print ""
    print "Non-Salted Source File Hashing:"
    print "HashCollider.py -md5 -file"
    print "\tAll values in the ListOfHashes.txt file will be passed to the non-salted MD5 handler."
    print "HashCollider.py -sha1 -file"
    print "\tAll values in the ListOfHashes.txt file will be passed to the non-salted SHA-1 handler."
    print "HashCollider.py -sha256 -file"
    print "\tAll values in the ListOfHashes.txt file will be passed to the non-salted SHA-256 handler."
    print "***Note for all non-salted source file hashing, "

#Application Code
def main():
    #Command line variable declarations
    commandLineArguments = []
    commandLineArgumentsLength = 0

    #Get command line arguments from the user
    commandLineArguments = sys.argv
    attributesObject = CommandLineArgumentsHandler.commandLineArgumentsHandler(commandLineArguments)
    if attributesObject.getTotalCommandLineArguments() == 3 and attributesObject.getCommandLineArgumentsValidated() \
            and attributesObject.getFileMode() != "-file":
        #Determine which operation mode to execute
        if attributesObject.getOperationMode() == "-md5" or attributesObject.getOperationMode() == "-sha1" \
                or attributesObject.getOperationMode() == "-sha256":
            printProgramHeader()

            #Pass the password to the appropriate hashing algorithm and being hashing
            print "Attempting to break hash..."
            startingCrackTime = TimingUtilities.getCPUClockTime()

            #Determine which algorithm to use
            if attributesObject.getOperationMode() == "-md5":
                collisionState = CollisionExecutionHandler.collisionExecutionHandler(
                    attributesObject.getPasswordHash(), "MD5", "FALSE")
            elif attributesObject.getOperationMode() == "-sha1":
                collisionState = CollisionExecutionHandler.collisionExecutionHandler(
                    attributesObject.getPasswordHash(), "SHA1", "FALSE")
            elif attributesObject.getOperationMode() == "-sha256":
                collisionState = CollisionExecutionHandler.collisionExecutionHandler(
                    attributesObject.getPasswordHash(), "SHA256", "FALSE")

            #Calculate the total cracking time
            endingCrackTime = TimingUtilities.getCPUClockTime()
            totalCrackTime = TimingUtilities.calculateDifference(startingCrackTime, endingCrackTime)

            #Determine if a collision occurred
            if collisionState[0]:
                #Determine if the user provided a username or not
                if attributesObject.getUsername() == "":
                    print "[NONE]\t\t" + "[" + collisionState[1] + "]"
                else:
                    print "[" + attributesObject.getUsername() + "]\t\t" + "[" + collisionState[1] + "]"
                print "Crack Completed in->",totalCrackTime,"seconds."
                print ""
                print "HashCollider.py exited successfully."
                sys.exit()
            elif collisionState[0] == False:
                print "An error occurred."
                print "Either the password wasn't in the master passwords list or "
                print "the password was encrypted using more advanced tactics."
        else:
            print attributesObject.getOperationMode()
            printUsage()
    elif attributesObject.getTotalCommandLineArguments() == 3 \
        and attributesObject.getFileMode() == "-file" and attributesObject.getCommandLineArgumentsValidated():
        printProgramHeader()

        #Parse the ListOfHashes.txt file
        applicationFileName = "ListOfHashes.txt"
        hashFileContents = FileIO.readFile(applicationFileName)

        #Iterate through hash file contents and attempt to break
        print "Attempting to break hashes..."
        for currentHashLine in hashFileContents:
            #Determine if the current line contains a username
            if "," not in currentHashLine:
                #If a username is not included with the password field, simply pass the current line
                #to the collision execution handler
                if attributesObject.getOperationMode() == "-md5":
                    collisionState = CollisionExecutionHandler.collisionExecutionHandler(currentHashLine,"MD5")
                elif attributesObject.getOperationMode() == "-sha1":
                    collisionState = CollisionExecutionHandler.collisionExecutionHandler(currentHashLine,"SHA1")
                elif attributesObject.getOperationMode() == "-sha256":
                    collisionState = CollisionExecutionHandler.collisionExecutionHandler(currentHashLine,"SHA256")
            else:
                #Split out the password hash if the comma is found
                currentUsernamePasswordSplit = currentHashLine.split(",")
                currentPasswordHash = currentUsernamePasswordSplit[1]
                currentPasswordHash = currentPasswordHash.rstrip()

                #If a username is is found with the password field, pass the split password
                #to the collision execution handler
                if attributesObject.getOperationMode() == "-md5":
                    collisionState = CollisionExecutionHandler.collisionExecutionHandler(currentPasswordHash,"MD5")
                elif attributesObject.getOperationMode() == "-sha1":
                    collisionState = CollisionExecutionHandler.collisionExecutionHandler(currentPasswordHash,"SHA1")
                elif attributesObject.getOperationMode() == "-sha256":
                    collisionState = CollisionExecutionHandler.collisionExecutionHandler(currentPasswordHash,"SHA256")

            #Determine if a collision occurred
            if collisionState[0]:
                #Determine if the user provided a username or not
                if "," not in currentHashLine:
                    usernameOutputLine = "[NONE]"
                    passwordOutputLine = "[" + collisionState[1] + "]"
                    outputLineFormatter = ('{0:15}{1:30}').format(usernameOutputLine, passwordOutputLine)
                    print outputLineFormatter
                else:
                    usernameOutputLine = "[" + currentUsernamePasswordSplit[0] + "]"
                    passwordOutputLine = "[" + collisionState[1] + "]"
                    outputLineFormatter = ('{0:15}{1:30}').format(usernameOutputLine, passwordOutputLine)
                    print outputLineFormatter
            elif collisionState[0] == False:
                if currentUsernamePasswordSplit[0] == "":
                    usernameOutputLine = "[NONE]"
                    passwordOutputLine = "[UNABLE OT BREAK HASH: " + currentPasswordHash + "]"
                    outputLineFormatter = ('{0:15}{1:30}').format(usernameOutputLine, passwordOutputLine)
                    print outputLineFormatter
                else:
                    usernameOutputLine = "[" + currentUsernamePasswordSplit[0] + "]"
                    passwordOutputLine = "[UNABLE TO BREAK HASH: " + currentPasswordHash + "]"
                    outputLineFormatter = ('{0:15}{1:30}').format(usernameOutputLine, passwordOutputLine)
                    print outputLineFormatter
    else:
        print "Command Line Arguments:",attributesObject.getCommandLineArgumentsValidated()
        print "File Mode:",attributesObject.getFileMode()
        printUsage()


main()