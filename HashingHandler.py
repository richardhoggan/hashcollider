#Import directives
import hashlib
import binascii

#Method Name: passwordEncryptionHandler()
#Method Description: Takes a password, salt, and number of rounds as input and returns the encrypted password.
def passwordEncryptionHandler(hashingDigest, passwordToEncrypt, encryptorSalt, numberOfRounds):
    #Variable declarations
    hashedPassword = ""

    #Validate that all arguments have values
    if hashingDigest == "md5" or hashingDigest == "sha1" or hashingDigest == "sha256" \
        and passwordToEncrypt != "" and encryptorSalt != "" and numberOfRounds != "":
        #Determine which hashing digest to use
        if hashingDigest == "md5":
            hashedObject = hashlib.pbkdf2_hmac('md5', passwordToEncrypt, encryptorSalt, numberOfRounds)
        elif hashingDigest == "sha1":
            hashedObject = hashlib.pbkdf2_hmac('sha1', passwordToEncrypt, encryptorSalt, numberOfRounds)
        elif hashingDigest == "sha256":
            hashedObject = hashlib.pbkdf2_hmac('sha256', passwordToEncrypt, encryptorSalt, numberOfRounds)

    #Get the hexlified version of the digest and return
    hashedPassword = binascii.hexlify(hashedObject)
    return hashedPassword

#Function Name: hashAsMD5()
#Function Description: Hashes the given password using the MD5 digest.
def hashAsMD5(currentPassword):
    #Varibale declarations
    hashingObject = hashlib.md5()

    if currentPassword != "":
        hashingObject.update(currentPassword)

    return hashingObject.hexdigest()

#Function Name: hashAsSHA1()
#Function Description: Hashes the given password using the SHA1 digest
def hashAsSHA1(currentPassword):
    #Variable declarations
    hashingObject = hashlib.sha1()

    if currentPassword != "":
        hashingObject.update(currentPassword)

    return hashingObject.hexdigest()

def hashAsSHA256(currentPassword):
    #variable decalrations
    hashingObject = hashlib.sha256()

    if currentPassword != "":
        hashingObject.update(currentPassword)

    return hashingObject.hexdigest()