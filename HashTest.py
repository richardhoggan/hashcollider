import hashlib

#Variable declarations
md5HashingObject = hashlib.md5()
sha1HashingObject = hashlib.sha1()
passwordClear = "password"
passwordMD5Hash = ""
passwordSHA1Hash = ""
passwordMD5HashLength = 0
passwordSHA1HashLength = 0

#Hash the password and get the length
md5HashingObject.update(passwordClear)
sha1HashingObject.update(passwordClear)
passwordMD5Hash = md5HashingObject.hexdigest()
passwordSHA1Hash = sha1HashingObject.hexdigest()
passwordMD5HashLength = len(passwordMD5Hash)
passwordSHA1HashLength = len(passwordSHA1Hash)

#Print the values
print "MD5 Hash:",passwordMD5Hash
print "SHA1 Hash:",passwordSHA1Hash
print "MD5 Length:",passwordMD5HashLength
print "SHA1 Length:",passwordSHA1HashLength