#Module Name: Analysis.py
#Module Developer: Rich Hoggan
#Module Creation Date:
#Module Description: Various functions for conducting analysis will attempting to break
#a given password or list of passwords.

#Method Name: calculateFactorial()
#Method Description: Calculates the factorial of the given n.
def calculateFactorial(n):
    factorial = 1
    for currentInt in range(1, n + 1):
        factorial *= currentInt
    return factorial

#Method Name: calculatePermutations()
#Method Description: Calculates the total permutations based on n and k.
def calculatePermutations(n,k):
    #Variable declarations
    nFactorial = 0
    kFactorial = 0
    nMinusK = 0
    nMinusKFactorial = 0
    totalPermutations = 0

    #Calcualte factorials
    nFactorial = calculateFactorial(n)
    kFactorial = calculateFactorial(k)
    nMinusK = n - k
    nMinusKFactorial = calculateFactorial(nMinusK)
    totalPermutations = nFactorial / (kFactorial * nMinusKFactorial)
    return totalPermutations

#Method Name: conductExpectedExecutionAnalysis()
#Method Description: Calcualtes the total expected executions given the total rounds,
#total passwords, and total salts.
def conductExpectedExecutionAnalysis(potentialRoundsList, potentialPasswordsList, potentialSaltsList):
    #Variable declarations
    potentialRoundsListLength = 0
    potentialPasswordsListLength = 0
    potentialSaltsListLength = 0
    totalEstimatedExecutions = 0

    #Get the lengths of all lists before continuing and validate that they all have data
    potentialRoundsListLength = len(potentialRoundsList)
    potentialPasswordsListLength = len(potentialPasswordsList)
    potentialSaltsListLength = len(potentialSaltsList)
    if potentialRoundsListLength > 0 and potentialPasswordsListLength > 0 \
        and potentialSaltsListLength > 0:
        #Calculate total permutations for each list
        totalSaltsPermutations = calculatePermutations(potentialSaltsListLength, 1)
        totalPasswordsPermutations = calculatePermutations(potentialPasswordsListLength, 1)
        totalRoundsPermutations = calculatePermutations(potentialRoundsListLength, 1)

        #calculate total estimated executions
        totalEstimatedExecutions = totalSaltsPermutations * totalPasswordsPermutations * totalRoundsPermutations

    #Return totalEsitmatedExecutions on completion
    return totalEstimatedExecutions

def conductExpectedExecutionAnalysisWithPasswords(hashList, potentialRoundsList,
    potentialPasswordsList, potentialSaltsList):
    #Variable declarations
    hashListLength = 0
    potentialRoundsListLength = 0
    potentialPasswordsListLength = 0
    potentialSaltsListLength = 0
    totalEstimatedExecutions = 0

    #Get the lengths of all lists before continuing and validate they have data
    hashListLength = len(hashList)
    potentialRoundsListLength = len(potentialRoundsList)
    potentialPasswordsListLength = len(potentialPasswordsList)
    potentialSaltsListLength = len(potentialSaltsList)

    if hashListLength > 0 and potentialRoundsListLength > 0 and potentialPasswordsListLength > 0 \
        and potentialSaltsListLength > 0:
        #Calculate total permutations for each list
        totalHashPermutations = calculatePermutations(hashListLength, 1)
        totalRoundsPermutations = calculatePermutations(potentialRoundsListLength, 1)
        totalPasswordsPermutations = calculatePermutations(potentialPasswordsListLength, 1)
        totalSaltsPermutations = calculatePermutations(potentialSaltsListLength, 1)

        #Calcualte total estimated executions
        totalEstimatedExecutions = totalHashPermutations * totalRoundsPermutations \
            * totalPasswordsPermutations * totalSaltsPermutations

    #Return totalEstimatedExecutions on completion
    return totalEstimatedExecutions
