import sys

def readFile(fileName):
    #Variable declarations
    fileHandle = None
    fileContents = []

    try:
        fileHandle = open(fileName,"r")
        for currentFileLine in fileHandle:
            fileContents.append(currentFileLine)
        fileHandle.close()
    except IOError:
        print "Unable to continue."
        print "The file provided was not readable or you don't have appropriate access permissions."
        print "Offending file:",fileName

    #Return fileContents on completion
    return fileContents